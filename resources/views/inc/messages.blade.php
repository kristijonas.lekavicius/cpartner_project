@if(count($errors)>0)
<div class="container">
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{$error}}
        </div>
    @endforeach
</div>
@endif

@if(session('success'))
    <div class="container">
        <div class="alert alert-success" role="alert">
            {{session('success')}}
        </div>
    </div>
@endif


@if(session('error'))
    <div class="container">
        <div class="alert alert-danger" role="alert">
            {{session('error')}}
        </div>
    </div>
@endif
