<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Praktikos projektas</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Kristijonas Lekavicius">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"   >
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-sm-10 col-10">
                    <h1>Your logo here</h1>
                </div>
                <div class="col-lg-2 col-sm-2 col-2 ">
                    <i class="fab fa-instagram"></i>
                    <i class="fab fa-facebook"></i>
                    <i class="fab fa-twitter-square"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-12">
                        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
                            <a class="navbar-brand" href="{{url('/')}}">Prekių Katalogas</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            <div class="navbar-collapse collapse w-100 order-1 order-md-0" id="navbarSupportedContent">
                
                                    <ul class="navbar-nav mr-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/')}}">Titulinis <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/')}}">Apie mus <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('/')}}">Kontaktai <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Prekes
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{url('prekes')}}">Visos prekės</a>
                                                <a class="dropdown-item" href="{{url('prekes/1')}}">Vaizdo plokštės</a>
                                                <a class="dropdown-item" href="{{url('prekes/2')}}">Žaidimai</a>
                                                <a class="dropdown-item" href="{{url('prekes/3')}}">Telefonai</a>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul  class="navbar-nav float-right">
                                    <li>
                                        <div class="dropdown">
                                                <button type="button" class="btn btn-info" data-toggle="dropdown">
                                                        <?php
                                                        $sum = 0;
                                                        ?>
                                                    @if(session('cart'))
                                                        @foreach(session('cart') as $id => $details)
                                                            <?php                                                            
                                                            $sum = $sum+$details['quantity'];
                                                            ?>
                                                        @endforeach
                                                     @endif
                                                     <i class="fas fa-shopping-cart" aria-hidden="true"></i> Krepšelis <span class="badge badge-pill badge-danger">{{$sum}}</span>
                                                </button>
                                                <div class="dropdown-menu">
                                                    
                                                    
                                                    @if(session('cart'))
                                                        @foreach(session('cart') as $id => $details)
                                                            <div class="row cart-detail">
                                                                <div class="col-lg-4 col-sm-4 col-4">
                                                                    <img src="{{ $details['photo'] }}" width="100" height="100"/>
                                                                </div>
                                                                <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                                                    <p>{{ $details['name'] }}</p>
                                                                    <span class="price text-info"> {{ $details['price'] }}&euro;</span> <span class="count">Kiekis:{{ $details['quantity'] }}</span>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                                                            <?php $total = 0 ?> 
                                                            @if(session('cart') != null) 
                                                                @foreach(session('cart') as $id => $details)
                                                                    <?php $total += $details['price'] * $details['quantity'] ?> 
                                                                @endforeach 
                                                            @endif
                                                            <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                                                <p>Suma: <span class="text-info"> {{ $total }}&euro;</span></p>
                                                            </div>
                                                            <a href="{{url('cart')}}" class="btn btn-primary btn-block">Visos prekės</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    </ul>
                            </div>
                            
                        </nav> 
                
            </div>
        </div>
    </header>
    
    <div class="container page">
        @include('inc.messages')
        @yield('content')
    </div>
    <footer>
        <div class="container">
            <div class="footer-holder">
                <ul class="footer-nav mb-0">
                    <li><a href="">lorem ipsum</a></li> 
                    <li><a href="">lorem ipsums</a></li> 
                    <li><a href="">lorem ipsum</a></li> 
                    <li><a href="">lorem ipsum</a></li> 
                    <li><a href="">lorem ipsum</a></li> 
                    <li><a href="">lorem ipsum</a></li>  
                </ul> 
                <ul class="footer-nav mb-0"> 
                    <li><a href="">lorem ipsum</a></li> 
                    <li><a href="">lorem ipsum</a></li> 
                    <li><a href="">lorem ipsum</a></li> 
                </ul> 
                <ul class="footer-nav mb-10"> 
                    <li class="text-center"><a href="">Lorem ipsum</a></li> 
                </ul>
                <div class="flex items-center justify-center footer__copy">
                    <p>&copy;2019 lorem ipsum</p>
                </div>
            </div>
        </div>
    </footer>
    @yield('scripts')
    
</body>
</html>
