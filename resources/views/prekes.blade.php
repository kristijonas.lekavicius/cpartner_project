@extends('master')

@section('content')
    <div class="row row-eq-height">
    @foreach($prekes as $key => $data)
    
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 products">  
            <div class="text-center"> 
                <img src="{{ $data->img }}" width="200" height="200">
                <div class="caption">
                    <h4>{{ $data->pav }}</h4>
                    <p><strong>Kaina: </strong> {{ $data->kaina }}&euro;</p>
                    <p class="btn-holder"><a href="{{ url('add-to-cart/'.$data->id) }}" class="btn btn-warning btn-lg" role="button">Į krepšelį</a> </p>
                </div>
            </div>
        </div>
    
    @endforeach 
    </div>  
{{ $prekes->links() }}  
@endsection 