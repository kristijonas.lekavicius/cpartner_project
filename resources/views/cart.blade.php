@extends('master')

@section('content')
<table id="cart" class="table table-responsive table-hover ">
        <thead>
        <tr>
            <th style="width:50%">Prekė</th>
            <th style="width:10%">Kaina</th>
            <th style="width:8%">Kiekis</th>
            <th style="width:22%" class="text-center">Bendra kaina</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
 
        <?php $total = 0 ?>
 
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
 
                <?php $total += $details['price'] * $details['quantity'] ?>
 
                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-3 hidden-xs"><img src="{{ $details['photo'] }}" width="100" height="100" class="img-responsive"/></div>
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">{{ $details['price'] }}&euro;</td>
                    <td data-th="Quantity">
                        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
                    </td>
                    <td data-th="Subtotal" class="text-center">{{ $details['price'] * $details['quantity'] }}&euro;</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-sync"></i></button>
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-alt"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif
 
        </tbody>
        <tfoot>
        <tr>
            <td><a href="{{ url('/prekes') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Atgal</a></td>
            <td colspan="2" class="hidden-xs"></td>
            <td><a href="{{ url('/destroy') }}" class="btn btn-warning"> Išvalyti krepšelį</a></td>
            <td class="hidden-xs text-center"><strong>Visa suma: {{ $total }}&euro;</strong></td>
        </tr>
        <tr>
            <td>
                <h5>Išsaugoti krepšelį vėliau:</h5>
                <input type="text" value="{{url('cart')}}/{{$query}}" id="myInput">
                    <button class="btn btn-warning" onclick="myFunction()" onmouseout="outFunc()">
                        <span class="tooltiptext" id="myTooltip">Kopijuoti url</span>
                    </button> 
            </td>
        </tr>
        </tfoot>
    </table>
@endsection 

@section('scripts')
    <script>
        function myFunction() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            document.execCommand("copy");

        }
        
        function outFunc() {
            var tooltip = document.getElementById("myTooltip");
            tooltip.innerHTML = "Kopijuoti url";
        }
    </script>
 
    <script type="text/javascript">
 
        $(".update-cart").click(function (e) {
           e.preventDefault();
 
           var ele = $(this);
 
            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });
 
        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
 
            var ele = $(this);
 
            if(confirm("Panaikinti prekę?")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });
 
    </script>
 
@endsection