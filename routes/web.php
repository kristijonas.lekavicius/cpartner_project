<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','CatalogController@index');

Route::get('/prekes','CatalogController@prekes');

Route::get('/prekes/{cid}','CatalogController@subprekes');

Route::get('/cart','CatalogController@cart');

Route::get('add-to-cart/{id}', 'CatalogController@addToCart');

Route::patch('update-cart', 'CatalogController@update');
 
Route::delete('remove-from-cart', 'CatalogController@remove');

Route::get('destroy', 'CatalogController@destroy');

Route::get('/cart','CatalogController@createlink');

Route::get('/cart/{url}','CatalogController@parseURL');

