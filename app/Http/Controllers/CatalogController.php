<?php

namespace App\Http\Controllers;
use App\Prekes;

use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index()
    {
        $prekes = Prekes::all();
 
        return view('home')->with('prekes',$prekes);
    }
    public function prekes()
    {
        $prekes = Prekes::orderBy('pav','asc')->paginate(4);
        return view('prekes')->with('prekes',$prekes);
    }
    public function subprekes($cid)
    {
        $prekes = Prekes::where('cid',$cid)->orderBy('pav','asc')->paginate(4);
        return view('prekes')->with('prekes',$prekes);
    }
    public function cart()
    {
        $prekes = Prekes::all();
        return view('cart')->with('prekes',$prekes);;
    }

    public function addToCart($id)
    {
        $prekes = Prekes::find($id);

        if(!$prekes) {

            abort(404);

        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                    $id => [
                        "name" => $prekes->pav,
                        "quantity" => 1,
                        "price" => $prekes->kaina,
                        "photo" => $prekes->img
                    ]
            ];

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Prekė sėkmingai pridėta į jūsų krepšelį!');
        }

        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {

            $cart[$id]['quantity']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Prekė sėkmingai pridėta į jūsų krepšelį!');

        }

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $prekes->pav,
            "quantity" => 1,
            "price" => $prekes->kaina,
            "photo" => $prekes->img
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Prekė sėkmingai pridėta į jūsų krepšelį!');
    }

    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
 
            $cart[$request->id]["quantity"] = $request->quantity;
 
            session()->put('cart', $cart);
 
            session()->flash('success', 'Krepšelis sėkmingai atnaujintas.');
        }
    }
 
    public function remove(Request $request)
    {
        if($request->id) {
 
            $cart = session()->get('cart');
 
            if(isset($cart[$request->id])) {
 
                unset($cart[$request->id]);
 
                session()->put('cart', $cart);
            }
 
            session()->flash('success', 'Prekė sėkmingai panaikinta.');
        }
    }

    public function destroy()
    {
        $cart = [];
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Prekės sėkmingai panaikintos.');
    }

    public function createlink(){
        $cart = session()->get('cart');
        if (!$cart) {
            $query = "";
            return view('cart',compact('query'));
        } else {
            foreach($cart as $arr => $a){
                $url[$arr] = $cart[$arr]["quantity"];
            }
            $query = http_build_query($url);
            return view('cart',compact('query'));
        }
    }

    public function parseURL($url){
        parse_str($url,$output);
        foreach($output as $arr => $a){
            $prekes = Prekes::find($arr);

            $cart[$arr] = [
            "name" => $prekes->pav,
            "quantity" => $a,
            "price" => $prekes->kaina,
            "photo" => $prekes->img
            ];
        }
        session()->put('cart', $cart);
        return redirect('cart');
    }
}
