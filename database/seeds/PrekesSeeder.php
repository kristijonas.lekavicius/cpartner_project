<?php

use Illuminate\Database\Seeder;

class PrekesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prekes')->insert([
            'pav' => 'Samsung A40 Silver',
            'body' => 'The Samsung Galaxy A40 is another introduction by Samsung in its A-series lineup. The device comes with the latest design and almost flagship level specs. It is made for those looking out for a bezel-less display with good performance.
            Body, Display, Battery, and Storage
            The Samsung Galaxy A40 is a unibody metal design with a glossy back that carries the dual camera setup and Samsung\'s logo. The front of the device is a beautiful 6.2-inch bezel-less Super AMOLED display with HD+ resolution resulting in a high 416 PPI density. The screen has a small waterdrop notch sitting above that sustains the front camera.
            The powerhouse of the device is a massive 4,000mAh battery, which is a must to power the at least for a day on a single charge. The smartphone with 64GB of inbuilt storage, which can be extended up to 256GB using a microSD card. 
            Configuration, Software, and Cameras
            The Samsung Galaxy A40 is powered by Samsung\'s own Exynos 7 Octa 7885 processor octa-core chipset that performs well when combined with 4GB RAM and renders decent photos after processing. The processor also has Mali-G71 GPU for better graphics performance.
            The device is running Google\'s Android 9.0 Pie out of the box with planned future upgrades. The main camera is a 13 MP + 8 MP dual camera setup that is built to take good portrait pictures. The front camera is an 8MP shooter, which though looks like an average one while considering the segment, but performs fairly well in good lighting conditions.
            Miscellaneous
            The Samsung Galaxy A40 retains all necessary hardware and ports like 3.5mm headphone jack and Type-C USB port. The device also carries all necessary sensors like a fingerprint sensor, light sensor, proximity sensor, accelerometer, and gyroscope. In terms of the connectivity, it comes with 4G VoLTE support, Wi-Fi 802.11, Mobile Hotspot, Bluetooth, GPS with A-GPS, Glonass, NFC, Mass storage device, USB charging, microUSB 2.0, etc.',
            'kaina' => 225.00,
            'cid' => 3,
            'img' => 'https://images.kainos24.lt/43/16/samsung-galaxy-a40.jpg',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'Apple iPhone 7 32GB Silver',
            'body' => 'The iPhone 7\'s overall design is similar to the iPhone 6S, but introduces new color options (matte black and jet black), water and dust resistance, a new capacitive, static home button, and removes the 3.5 mm headphone jack. The device\'s internal hardware also received upgrades, including a heterogeneous quad-core system-on-chip with improved system and graphics performance, and upgraded 12 megapixel rear-facing cameras with optical image stabilization on all models and an additional telephoto lens on the iPhone 7 Plus model to provide enhanced zoom capabilities.',
            'kaina' => 398.00,
            'cid' => 3,
            'img' => 'https://images.kainos24.lt/43/87/apple-iphone-7-32gb.jpg',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'Huawei P30 Pro Dual 128GB Aurora Blue',
            'body' => 'Ieškantiems dar didesnių galimybių ir maksimalios galios Huawei paruošė P30 PRO versiją. Jeigu jau spėjote pamanyti, kad joks kitas telefonas nebegali fotografuoti geriau už P30, galvokite iš naujo. P30 PRO kamera jums siūlo praktiškai neribotas galimybes. Be to, stiliaus ir patogumo etalonu jau seniai esanti P serija naujajame modelyje pasiekia tobulybę. P30 PRO – tai telefonas, atkeliavęs pas jus tiesiai iš ateities.',
            'kaina' => 805.00 ,
            'cid' => 3,
            'img' => 'https://www.kmobiles.lt/public_files/images/20190327144434_HUAWEI-P30-Pro-6-47-Inch-8GB-128GB-Smartphone-Aurora-848272-.jpg
            ',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'Gigabyte GeForce RTX 2080 Ti WINDFORCE 11GB GDDR6',
            'body' => 'NVIDIA’s newest flagship graphics card is a revolution in gaming realism and performance. Its powerful NVIDIA Turing™ GPU architecture, breakthrough technologies, and 11 GB of next-gen, ultra-fast GDDR6 memory make it the world’s ultimate gaming GPU. ',
            'kaina' => 1205.00 ,
            'cid' => 1,
            'img' => 'http://www.skytech.lt/images/large/22/2034222.png',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'MSI RADEON VII 16G, 16GB HBM2, 3xDP, HDMI',
            'body' => 'AMD’s Radeon VII is the company’s eagerly anticipated response to the RTX 2080 graphics cards that Nvidia launched last fall. Priced the same as the RTX 2080, the Radeon VII is the first graphics card underpinned by a 7nm process, potentially giving AMD the ammunition to deliver a competitive high-end graphics card for the first time in years.
            The VII is the direct successor to AMD’s Vega 64 and shares a similar architecture to that card, with specific improvements in clock speeds and VRAM.',
            'kaina' => 776.00,
            'cid' => 1,
            'img' => 'http://www.skytech.lt/images/large/33/2241433.png',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'ASUS GeForce GTX 1660 Phoenix, 6GB GDDR5, HDMI, DVI, DP',
            'body' => 'The GeForce® GTX 1660 Ti and 1660 are built with the breakthrough graphics performance of the award-winning NVIDIA Turing™ architecture. Easily upgrade your PC and get performance that rivals the GeForce GTX 1070, a 16 Series GPU is a blazing-fast supercharger for today’s most popular games, and even faster with modern titles.',
            'kaina' => 250.29,
            'cid' => 1,
            'img' => 'http://www.skytech.lt/images/large/10/2232810.png ',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'ASUS Dual GeForce GTX 1660 Ti OC EVO, 6GB GDDR6, 2xHDMI, DVI, DP',
            'body' => 'The GeForce® GTX 1660 Ti and 1660 are built with the breakthrough graphics performance of the award-winning NVIDIA Turing™ architecture. Easily upgrade your PC and get performance that rivals the GeForce GTX 1070, a 16 Series GPU is a blazing-fast supercharger for today’s most popular games, and even faster with modern titles.',
            'kaina' => 341.99,
            'cid' => 1 ,
            'img' => 'http://www.skytech.lt/images/large/11/2284911.jpg',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'Far Cry 5 ',
            'body' => 'In its latest installment, the award-winning Far Cry series will take you to America.Welcome to Hope County, Montana. This idyllic area is the home of people who love freedom - and a fanatical sect that claims the end of the world known as the Project at the Gates of Eden. The sectarians are headed by the charismatic prophet Jospeh Seed with his loyal siblings - heralds. The Gate of Eden permeates quietly every aspect of everyday life in this once peaceful town. Your arrival becomes an impulse for the sect to aggressively take control of the region, so you must resist and light a fire of resistance in order to free the oppressed population.Unlimited, explore the rivers, terrain and airspace of Hope County, with the largest selection of customizable weapons and vehicles in the history of Far Cry games. You are the main character of a story set in an exciting world that does not turn the other cheek. The places and allies discovered by you will shape your adventure in a way you are not expecting.',
            'kaina' => 44.99,
            'cid' => 2,
            'img' => 'http://www.skytech.lt/images/large/91/1757391.jpg',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'Battlefield V',
            'body' => 'The Battlefield series goes back to its roots in a never-before-seen portrayal of World War 2. Take on physical, all-out multiplayer with your squad in modes like the vast Grand Operations and the cooperative Combined Arms, or witness human drama set against global combat in the single player War Stories. As you fight in epic, unexpected locations across the globe, enjoy the richest and most immersive Battlefield yet. Now also includes Firestorm – Battle Royale, reimagined for Battlefield.
            ',
            'kaina' => 43.59,
            'cid' =>  2,
            'img' => 'http://www.skytech.lt/images/large/17/2020617.jpg',
        ]);
        DB::table('prekes')->insert([
            'pav' => 'Crash Bandicoot N. Sane Trilogy',
            'body' => 'Your favorite marsupial, Crash Bandicoot™, is back! He’s enhanced, entranced and ready-to-dance with the N. Sane Trilogy game collection. Now you can experience Crash Bandicoot like never before. Spin, jump, wump and repeat as you take on the epic challenges and adventures through the three games that started it all, Crash Bandicoot™, Crash Bandicoot™ 2: Cortex Strikes Back and Crash Bandicoot™ 3: Warped. Relive all your favorite Crash moments in their fully-remastered graphical glory and get ready to put some UMPH in your WUMP!',
            'kaina' => 42.29,
            'cid' =>  2,
            'img' => 'http://www.skytech.lt/images/large/45/1850745.jpg',
        ]);
    }
}
