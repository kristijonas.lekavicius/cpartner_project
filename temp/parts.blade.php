@extends('master')

@section('content')
    @foreach($prekes as $key => $data)
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="text-center"> 
                <img src="{{ $data->img }}" width="200" height="200">
                <div class="caption">
                    <h4>{{ $data->pav }}</h4>
                    <p><strong>Kaina: </strong> {{ $data->kaina }}&euro;</p>
                    <p class="btn-holder"><a href="{{ url('add-to-cart/'.$data->id) }}" class="btn btn-warning" role="button">Į krepšelį</a> </p>
                </div>
            </div>
        </div>
    @endforeach 
@endsection 